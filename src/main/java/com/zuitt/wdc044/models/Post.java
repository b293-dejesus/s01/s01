package com.zuitt.wdc044.models;


import javax.persistence.*;
import javax.persistence.Table;



@Entity
@Table(name="posts")
public class Post {

    @Id
    @GeneratedValue

    private Long id;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column
    private String title;


    @Column
    private String content;


    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private  User user;


    public Post(){}

    public Post(String title,String content){

        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return  title;
    }

    public String getContent(){
        return content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }



    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

