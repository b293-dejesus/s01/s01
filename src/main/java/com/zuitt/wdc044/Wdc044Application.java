package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



//This file serves as our index/entry point

//@SpringBootApplication this is called as "Annotation" Mark
//represented by the @ symbol
//This is a shortcut or method we manually create or describe our purpose or configure a framework
//Annotations are used to provide supplemental information about the program

@SpringBootApplication

//Tells the springboot that this will handle endpoints for web requests
@RestController
//To handle endpoints and methods

public class Wdc044Application {
	//This method starts the whole Springboot Framework
	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	//How to run our application?
	// ./mvnw spring-boot:run

	//Mapping HTTP Get Request
	@GetMapping("/hello")

	//@RequestParam is used to extract query parameters, form parameters, and even files from the request
	///name = Cardo; Hello Cardo
	//name = World, Hello World
	//"?" means the start of the parameters followed by the "key=value" pair

	//Access query parameter
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet", defaultValue = "World") String greet){
		return String.format("Good evening, %s! Welcome to Batch 293!", greet);
	}

//	Activity 1

	@GetMapping("/greetings2")
	public String greetings2(@RequestParam(value = "greet2", defaultValue = "User") String greet2){
		return String.format("Hi, %s! ", greet2);
	}


	@GetMapping("/nameAge")
	public String getNameAndAge(
			@RequestParam(name = "age", defaultValue = "18") int age,
			@RequestParam(name = "name", defaultValue = "Juan") String name) {

		return String.format("Hello %s! Your age is %d.", name, age);
	}



}
